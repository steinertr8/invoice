﻿#pragma checksum "..\..\Einzahlung.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "A5233FF413C4BE0F2633508046B2B1B9288CC363FB69924B9BC4A2B23EFAE027"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using AbrechungsProgramm;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AbrechungsProgramm {
    
    
    /// <summary>
    /// Einzahlung
    /// </summary>
    public partial class Einzahlung : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LeihUndRuezahlungGrid;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RowDefinition grid1;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EinzahlungBuchen;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label1Betrag;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label2Betrag;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label3Betrag;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label4Betrag;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label2An;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label3An;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Label4An;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox EinzahlungBeguenstigter;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox EinzahlungBeguenstigter2;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox EinzahlungBeguenstigter3;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox EinzahlungBeguenstigter4;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox EinzahlungSchuldner;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Notiz;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EinzahlungBetrag;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EinzahlungBetrag1;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EinzahlungBetrag2;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EinzahlungBetrag3;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\Einzahlung.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EinzahlungBetrag4;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AbrechungsProgramm;component/einzahlung.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Einzahlung.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LeihUndRuezahlungGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.grid1 = ((System.Windows.Controls.RowDefinition)(target));
            return;
            case 3:
            this.EinzahlungBuchen = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\Einzahlung.xaml"
            this.EinzahlungBuchen.Click += new System.Windows.RoutedEventHandler(this.EinzahlungBuchenUpdate);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 40 "..\..\Einzahlung.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BackHome);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 59 "..\..\Einzahlung.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.WeiterePersonenEinsetzen);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Label1Betrag = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.Label2Betrag = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.Label3Betrag = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.Label4Betrag = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.Label2An = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.Label3An = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.Label4An = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.EinzahlungBeguenstigter = ((System.Windows.Controls.ComboBox)(target));
            
            #line 91 "..\..\Einzahlung.xaml"
            this.EinzahlungBeguenstigter.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EinzahlungBeguenstigter_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 14:
            this.EinzahlungBeguenstigter2 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 92 "..\..\Einzahlung.xaml"
            this.EinzahlungBeguenstigter2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EinzahlungBeguenstigter2_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            this.EinzahlungBeguenstigter3 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 93 "..\..\Einzahlung.xaml"
            this.EinzahlungBeguenstigter3.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EinzahlungBeguenstigter3_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.EinzahlungBeguenstigter4 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 94 "..\..\Einzahlung.xaml"
            this.EinzahlungBeguenstigter4.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EinzahlungBeguenstigter4_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.EinzahlungSchuldner = ((System.Windows.Controls.ComboBox)(target));
            
            #line 95 "..\..\Einzahlung.xaml"
            this.EinzahlungSchuldner.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EinzahlungSchuldnerAuswahl);
            
            #line default
            #line hidden
            return;
            case 18:
            this.Notiz = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.EinzahlungBetrag = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.EinzahlungBetrag1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.EinzahlungBetrag2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.EinzahlungBetrag3 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.EinzahlungBetrag4 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            
            #line 102 "..\..\Einzahlung.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.WeiterePersonenLoeschen);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

