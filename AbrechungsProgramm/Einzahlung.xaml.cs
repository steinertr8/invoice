﻿using AbrechungsProgramm.SQlVerbund;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AbrechungsProgramm
{
    /// <summary>
    /// Interaktionslogik für Einzahlung.xaml
    /// </summary>
    public partial class Einzahlung : Window
    {
        // Zähler fuer Visibilty Zählt die Event von "+" und "-" Buttons
        private int zaehler = 0;
        public Einzahlung()
        {
            InitializeComponent();
            AkualisiereComboBox();
            Verstecken();
        }
        private void AkualisiereComboBox()
        {

            EinzahlungSchuldner.Items.Clear();
            EinzahlungBeguenstigter.Items.Clear();
            EinzahlungBeguenstigter2.Items.Clear();
            EinzahlungBeguenstigter3.Items.Clear();
            EinzahlungBeguenstigter4.Items.Clear();
            string query1 = "select Name From Reisende";

            SqlVerbundung ver = new SqlVerbundung();
            ver.VerSQLOpen();
            SqlDataReader reader1 = ver.VerReader(query1);
            while (reader1.Read())
            {
                //MessageBox.Show(reader1["Name"].ToString());
                EinzahlungSchuldner.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter2.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter3.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter4.Items.Add(reader1["Name"]);
            }
            ver.VerSQLClose();
        }

        private int EinzahlungBuchenUpdateParamName(string namme)
        {
            int id = 0;
            string query1 = "exec dbo.ups_Name @Name";
            SqlVerbundung ver1 = new SqlVerbundung();
            ver1.VerSQLOpen();
            SqlDataReader reader1 = ver1.VerReaderName(query1, namme);
            while (reader1.Read())
            {
                id = (int)reader1["UserID"];
            }
            ver1.VerSQLClose();

            return id;
        }
        //Abfrage SQL Notiz aus der Tabelle Betrag
        private int EinzahlungBuchenUpdateParamNotiz(string notiz)
        {
            int id = 0;
            string query3 = "exec dbo.ups_Notiz  @Notiz";
            SqlVerbundung VerbindungNotiz = new SqlVerbundung();
            VerbindungNotiz.VerSQLOpen();
            SqlDataReader reader3 = VerbindungNotiz.VerReaderNotiz(query3, notiz);
            while (reader3.Read())
            {
                id = (int)reader3["BetragID"];
            }
            VerbindungNotiz.VerSQLClose();
            return id;
        }

        // Berechung EigabeKontrolle
        //Abfrage SQL UserID aus der Tabelle Reisende


        private bool BerchnungZweiPersonen(double wert, double wert1, double wert2)
        {
            bool gama = false;

            if ((wert - (wert1 + wert2)) == 0)
            {
                gama = true;
            }

            return gama;
        }
        private bool BerchnungDreiPersonen(double wert, double wert1, double wert2, double wert3)
        {
            bool gama = false;

            if ((wert - (wert1 + wert2 + wert3)) == 0)
            {
                gama = true;
            }

            return gama;
        }
        private bool BerchnungvierPersonen(double wert, double wert1, double wert2, double wert3, double wert4)
        {
            bool gama = false;

            if ((wert - (wert1 + wert2 + wert3 + wert4)) == 0)
            {
                gama = true;
            }

            return gama;
        }
        private void EinzahlungBuchenUpdate(object sender, RoutedEventArgs e)
        {
            //Zähler hier wird er auch gebraucht um zu deferenzieren
            if (zaehler == 0)
            {
                // Variablen von Eingabe Feldern TexBox und ComboBox
                // Funktionen ausgelagert  
                string einzahlungsN = Notiz.Text.ToString();
                int BetragNotizInt = EinzahlungBuchenUpdateParamNotiz(einzahlungsN);
                string einzahlungsS = EinzahlungSchuldner.SelectedValue.ToString();
                int ID = EinzahlungBuchenUpdateParamName(einzahlungsS);
                string einzahlungsB = EinzahlungBeguenstigter.SelectedValue.ToString();
                int ID1 = EinzahlungBuchenUpdateParamName(einzahlungsB);

                // Nachfolgend ist das Betrag von 
                double wert = double.Parse(EinzahlungBetrag.Text);

                if (EinzahlungBeguenstigter.SelectedValue != null && EinzahlungSchuldner.SelectedValue != null && EinzahlungBetrag.Text != "" && Notiz.Text != "")
                {
                    if (ID != 0 && ID1 != 0 && wert != 0 && BetragNotizInt != 0)
                    {
                        try // Rueckzahlung
                        {
                            SqlVerbundung verbindung = new SqlVerbundung();
                            verbindung.VerSQLOpen();
                            string query = "exec usr_AuszugDatumMitNotizID @ID1, @ID2, @Betrag, @FK_BetragID";
                            verbindung.UpdateMitFK_BetragID(query, ID, ID1, wert, BetragNotizInt);
                            verbindung.VerSQLClose();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            return;
                        }
                    }
                    if (ID != 0 && ID1 != 0 && wert != 0 && BetragNotizInt == 0 && einzahlungsN != "")
                    {
                        try // Leihen
                        {
                            SqlVerbundung verbindung1 = new SqlVerbundung();
                            verbindung1.VerSQLOpen();
                            string query = "exec usr_AuszugDatumOhneNotizID @ID1, @ID2, @Betrag, @Notiz";
                            verbindung1.VerexecOhneNotizID(query, ID, ID1, wert, einzahlungsN);
                            verbindung1.VerSQLClose();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            return;
                        }
                    }
                }
            }
            if (zaehler == 1)
            {
                // Auf Personen Geteilt die Beträge die man verteilt
                // Variablen von Eingabe Feldern TexBox und ComboBox
                string einzahlungsN = Notiz.Text.ToString();
                int BetragNotizInt = EinzahlungBuchenUpdateParamNotiz(einzahlungsN);

                string einzahlungsS = EinzahlungSchuldner.SelectedValue.ToString();
                int ID = EinzahlungBuchenUpdateParamName(einzahlungsS);
                string einzahlungsB = EinzahlungBeguenstigter.SelectedValue.ToString();
                int ID1 = EinzahlungBuchenUpdateParamName(einzahlungsB);
                double wert = double.Parse(EinzahlungBetrag.Text);
                double wert1 = double.Parse(EinzahlungBetrag1.Text);

                if (ID != 0 && ID1 != 0 && wert != 0 && wert1 != 0 && BetragNotizInt == 0 && einzahlungsN != "")
                {
                    try
                    {
                        SqlVerbundung verbindung1 = new SqlVerbundung();
                        verbindung1.VerSQLOpen();
                        string query = "exec usr_AuszugDatumOhneNotizID @ID, @ID1, @Betrag,@Betrag1, @Notiz";
                        verbindung1.VerexecOhneNotizIDEiner(query, ID, ID1, wert, wert1, einzahlungsN);
                        verbindung1.VerSQLClose();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }
            }
            if (zaehler == 2)
            {
                // Variablen von Eingabe Feldern TexBox und ComboBox
                string einzahlungsN = Notiz.Text.ToString();
                int BetragNotizInt = EinzahlungBuchenUpdateParamNotiz(einzahlungsN);

                string einzahlungsS = EinzahlungSchuldner.SelectedValue.ToString();
                int ID = EinzahlungBuchenUpdateParamName(einzahlungsS);
                string einzahlungsB = EinzahlungBeguenstigter.SelectedValue.ToString();
                int ID1 = EinzahlungBuchenUpdateParamName(einzahlungsB);
                string zwei = EinzahlungBeguenstigter2.SelectedValue.ToString();
                int ID2 = EinzahlungBuchenUpdateParamName(zwei);

                double wert = double.Parse(EinzahlungBetrag.Text);
                double wert1 = double.Parse(EinzahlungBetrag1.Text);
                double wert2 = double.Parse(EinzahlungBetrag2.Text);
                bool summe = BerchnungZweiPersonen(wert, wert1, wert2);
                if (ID != 0 && ID1 != 0 && ID2 != 0 && wert != 0 && wert1 != 0 && wert2 != 0 && BetragNotizInt == 0 && einzahlungsN != "" && summe == true)
                {
                    try
                    {
                        SqlVerbundung verbindung1 = new SqlVerbundung();
                        verbindung1.VerSQLOpen();
                        string query = "exec usr_VereinzelteVeerteilungZwei @ID, @ID1, @ID2, @Betrag, @Betrag1, @Betrag2, @Notiz";
                        verbindung1.VereinzelteVeerteilungZwei(query, ID, ID1, ID2, wert, wert1, wert2, einzahlungsN);
                        verbindung1.VerSQLClose();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }

            }
            if (zaehler == 3)
            {
                // Variablen von Eingabe Feldern TexBox und ComboBox
                string einzahlungsN = Notiz.Text.ToString();
                int BetragNotizInt = EinzahlungBuchenUpdateParamNotiz(einzahlungsN);

                string einzahlungsS = EinzahlungSchuldner.SelectedValue.ToString();
                int ID = EinzahlungBuchenUpdateParamName(einzahlungsS);
                string einzahlungsB = EinzahlungBeguenstigter.SelectedValue.ToString();
                int ID1 = EinzahlungBuchenUpdateParamName(einzahlungsB);
                string zwei = EinzahlungBeguenstigter2.SelectedValue.ToString();
                int ID2 = EinzahlungBuchenUpdateParamName(zwei);
                string drei = EinzahlungBeguenstigter3.SelectedValue.ToString();
                int ID3 = EinzahlungBuchenUpdateParamName(drei);

                double wert = double.Parse(EinzahlungBetrag.Text);
                double wert1 = double.Parse(EinzahlungBetrag1.Text);
                double wert2 = double.Parse(EinzahlungBetrag2.Text);
                double wert3 = double.Parse(EinzahlungBetrag3.Text);
                bool summe = BerchnungDreiPersonen(wert, wert1, wert2, wert3);
                if (ID != 0 && ID1 != 0 && ID2 != 0 && ID3 != 0 && wert != 0 && wert1 != 0 && wert2 != 0 && wert3 != 0 && BetragNotizInt == 0 && einzahlungsN != "" && summe == true)
                {
                    try
                    {
                        SqlVerbundung verbindung1 = new SqlVerbundung();
                        verbindung1.VerSQLOpen();
                        string query = "exec usr_VereinzelteVeerteilungZwei @ID, @ID1, @ID2, @ID3, @Betrag, @Betrag1, @Betrag2, @Betrag3, @Notiz";
                        verbindung1.VereinzelteVeerteilungDrei(query, ID, ID2, ID2, ID3, wert, wert1, wert2, wert3, einzahlungsN);
                        verbindung1.VerSQLClose();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }
            }
            if (zaehler == 4)
            {
                // Variablen von Eingabe Feldern TexBox und ComboBox
                string einzahlungsN = Notiz.Text.ToString();
                int BetragNotizInt = EinzahlungBuchenUpdateParamNotiz(einzahlungsN);

                string einzahlungsS = EinzahlungSchuldner.SelectedValue.ToString();
                int ID = EinzahlungBuchenUpdateParamName(einzahlungsS);
                string einzahlungsB = EinzahlungBeguenstigter.SelectedValue.ToString();
                int ID1 = EinzahlungBuchenUpdateParamName(einzahlungsB);
                string zwei = EinzahlungBeguenstigter2.SelectedValue.ToString();
                int ID2 = EinzahlungBuchenUpdateParamName(zwei);
                string drei = EinzahlungBeguenstigter3.SelectedValue.ToString();
                int ID3 = EinzahlungBuchenUpdateParamName(drei);
                string vier = EinzahlungBeguenstigter4.SelectedValue.ToString();
                int ID4 = EinzahlungBuchenUpdateParamName(vier);

                double wert = double.Parse(EinzahlungBetrag.Text);
                double wert1 = double.Parse(EinzahlungBetrag1.Text);
                double wert2 = double.Parse(EinzahlungBetrag2.Text);
                double wert3 = double.Parse(EinzahlungBetrag3.Text);
                double wert4 = double.Parse(EinzahlungBetrag4.Text);

                //Prühfe sind die Betraege korrekt eingegeben
                bool summe = BerchnungvierPersonen(wert, wert1, wert2, wert3, wert4);

                if (ID != 0 && ID1 != 0 && ID2 != 0 && ID3 != 0 && ID4 != 0 && wert != 0 && wert1 != 0 && wert2 != 0 && wert3 != 0 && wert4 != 0 && BetragNotizInt == 0 && einzahlungsN != "" && summe == true)
                {
                    try
                    {
                        SqlVerbundung verbindung1 = new SqlVerbundung();
                        verbindung1.VerSQLOpen();
                        string query = "exec usr_VereinzelteVeerteilungVier @ID, @ID1, @ID2, @ID3, @ID4, @Betrag, @Betrag1, @Betrag2, @Betrag3, @Betrag4, @Notiz";
                        verbindung1.VereinzelteVeerteilungVier(query, ID, ID1, ID2, ID3, ID4, wert, wert1, wert2, wert3, wert4, einzahlungsN);
                        verbindung1.VerSQLClose();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Ups da hat etwas nicht geklappt ", "Info");
            }
            // Wechsel auf die Andere Seite
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }

        //Visibility und 
        private void WeiterePersonenEinsetzen(object sender, RoutedEventArgs e)
        {
            if (zaehler <= 5 && zaehler >= 0)
            {
                zaehler += 1;

                if (zaehler == 1)
                {
                    Label1Betrag.Visibility = Visibility.Visible;
                    EinzahlungBetrag1.Visibility = Visibility.Visible;
                }

                if (zaehler == 2)
                {
                    Label1Betrag.Visibility = Visibility.Visible;
                    EinzahlungBetrag1.Visibility = Visibility.Visible;
                    Label2An.Visibility = Visibility.Visible;
                    EinzahlungBeguenstigter2.Visibility = Visibility.Visible;
                    Label2Betrag.Visibility = Visibility.Visible;
                    EinzahlungBetrag2.Visibility = Visibility.Visible;
                }
                if (zaehler == 3)
                {
                    Label3An.Visibility = Visibility.Visible;
                    EinzahlungBeguenstigter3.Visibility = Visibility.Visible;
                    Label3Betrag.Visibility = Visibility.Visible;
                    EinzahlungBetrag3.Visibility = Visibility.Visible;
                }
                if (zaehler == 4)
                {
                    Label4An.Visibility = Visibility.Visible;
                    EinzahlungBeguenstigter4.Visibility = Visibility.Visible;
                    Label4Betrag.Visibility = Visibility.Visible;
                    EinzahlungBetrag4.Visibility = Visibility.Visible;
                }
                if (zaehler == 5)
                {
                    zaehler -= 1;
                    MessageBox.Show("Hoer doch auf", "Info");
                }
            }
        }

        private void WeiterePersonenLoeschen(object sender, RoutedEventArgs e)
        {

            if (zaehler <= 5 && zaehler > 0)
            {
                if (zaehler == 1)
                {
                    Label1Betrag.Visibility = Visibility.Hidden;
                    EinzahlungBetrag1.Visibility = Visibility.Hidden;
                }
                if (zaehler == 2)
                {
                    Label2An.Visibility = Visibility.Hidden;
                    EinzahlungBeguenstigter2.Visibility = Visibility.Hidden;
                    Label2Betrag.Visibility = Visibility.Hidden;
                    EinzahlungBetrag2.Visibility = Visibility.Hidden;
                }
                if (zaehler == 3)
                {
                    Label3An.Visibility = Visibility.Hidden;
                    EinzahlungBeguenstigter3.Visibility = Visibility.Hidden;
                    Label3Betrag.Visibility = Visibility.Hidden;
                    EinzahlungBetrag3.Visibility = Visibility.Hidden;
                }
                if (zaehler == 4)
                {
                    Label4An.Visibility = Visibility.Hidden;
                    EinzahlungBeguenstigter4.Visibility = Visibility.Hidden;
                    Label4Betrag.Visibility = Visibility.Hidden;
                    EinzahlungBetrag4.Visibility = Visibility.Hidden;
                }
                zaehler -= 1;
            }
            if (zaehler == 5)
            {
                zaehler -= 1;
                MessageBox.Show("Hoer doch auf", "Info");
            }

        }
        private void Verstecken()
        {
            Label1Betrag.Visibility = Visibility.Hidden;
            EinzahlungBetrag1.Visibility = Visibility.Hidden;
            Label2An.Visibility = Visibility.Hidden;
            EinzahlungBeguenstigter2.Visibility = Visibility.Hidden;
            Label2Betrag.Visibility = Visibility.Hidden;
            EinzahlungBetrag2.Visibility = Visibility.Hidden;
            Label3An.Visibility = Visibility.Hidden;
            EinzahlungBeguenstigter3.Visibility = Visibility.Hidden;
            Label3Betrag.Visibility = Visibility.Hidden;
            EinzahlungBetrag3.Visibility = Visibility.Hidden;
            Label4An.Visibility = Visibility.Hidden;
            EinzahlungBeguenstigter4.Visibility = Visibility.Hidden;
            Label4Betrag.Visibility = Visibility.Hidden;
            EinzahlungBetrag4.Visibility = Visibility.Hidden;
        }
        private void BackHome(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }
        // CheckBox auswahl loescht den ausgewaehlte fuer die Nachfolgende Checkbox 
        private void EinzahlungSchuldnerAuswahl(object sender, SelectionChangedEventArgs e)
        {
           // EinzahlungSchuldner.Items.Clear();
            EinzahlungBeguenstigter.Items.Clear();
            EinzahlungBeguenstigter2.Items.Clear();
            EinzahlungBeguenstigter3.Items.Clear();
            EinzahlungBeguenstigter4.Items.Clear();

            string query1 = "select Name From Reisende";
            SqlVerbundung Verbindung = new SqlVerbundung();
            Verbindung.VerSQLOpen();
            SqlDataReader reader1 = Verbindung.VerReader(query1);
            MessageBox.Show("hhrrr");
            while (reader1.Read())
            {
                //EinzahlungSchuldner.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter2.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter3.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter4.Items.Add(reader1["Name"]);
            }
            Verbindung.VerSQLClose();
            foreach (object item in EinzahlungBeguenstigter.Items)
            {
                if (item.Equals(EinzahlungSchuldner.SelectedValue))
                {
                    EinzahlungBeguenstigter.Items.Remove(item);

                    foreach (object item1 in EinzahlungBeguenstigter2.Items)
                    {
                        if (item1.Equals(EinzahlungSchuldner.SelectedValue))
                        {
                            EinzahlungBeguenstigter2.Items.Remove(item1);

                            foreach (object item2 in EinzahlungBeguenstigter3.Items)
                            {
                                if (item2.Equals(EinzahlungSchuldner.SelectedValue))
                                {
                                    EinzahlungBeguenstigter3.Items.Remove(item2);

                                    foreach (object item3 in EinzahlungBeguenstigter4.Items)
                                    {
                                        if (item3.Equals(EinzahlungSchuldner.SelectedValue))
                                        {
                                            EinzahlungBeguenstigter4.Items.Remove(item3);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }

        }
        // CheckBox auswahl loescht den ausgewaehlte fuer die Nachfolgende Checkbox
        private void EinzahlungBeguenstigter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           // EinzahlungSchuldner.Items.Clear();
            EinzahlungBeguenstigter2.Items.Clear();
            EinzahlungBeguenstigter3.Items.Clear();
            EinzahlungBeguenstigter4.Items.Clear();

            string query1 = "select Name From Reisende";
            SqlVerbundung Verbindung = new SqlVerbundung();
            Verbindung.VerSQLOpen();
            SqlDataReader reader1 = Verbindung.VerReader(query1);
            while (reader1.Read())
            {
                EinzahlungBeguenstigter2.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter3.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter4.Items.Add(reader1["Name"]);
            }
            Verbindung.VerSQLClose();
            foreach (object item in EinzahlungBeguenstigter2.Items)
            {
                if (item.Equals(EinzahlungBeguenstigter.SelectedValue))
                {
                    EinzahlungBeguenstigter2.Items.Remove(item);

                    foreach (object item1 in EinzahlungBeguenstigter3.Items)
                    {
                        if (item1.Equals(EinzahlungBeguenstigter.SelectedValue))
                        {
                            EinzahlungBeguenstigter3.Items.Remove(item1);

                            foreach (object item2 in EinzahlungBeguenstigter4.Items)
                            {
                                if (item2.Equals(EinzahlungBeguenstigter.SelectedValue))
                                {
                                    EinzahlungBeguenstigter4.Items.Remove(item2);

                                    foreach (object item3 in EinzahlungBeguenstigter2.Items)
                                    {
                                        if (item3.Equals(EinzahlungSchuldner.SelectedValue))
                                        {
                                            EinzahlungBeguenstigter2.Items.Remove(item3);

                                            foreach (object item4 in EinzahlungBeguenstigter3.Items)
                                            {
                                                if (item4.Equals(EinzahlungSchuldner.SelectedValue))
                                                {
                                                    EinzahlungBeguenstigter3.Items.Remove(item4);

                                                    foreach (object item5 in EinzahlungBeguenstigter4.Items)
                                                    {
                                                        if (item5.Equals(EinzahlungSchuldner.SelectedValue))
                                                        {
                                                            EinzahlungBeguenstigter4.Items.Remove(item5);

                                                            break;
                                                        }
                                                    }
                                                    break;
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }

        }
        // CheckBox auswahl loescht den ausgewaehlte fuer die Nachfolgende Checkbox
        private void EinzahlungBeguenstigter2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EinzahlungBeguenstigter3.Items.Clear();
            EinzahlungBeguenstigter4.Items.Clear();

            string query1 = "select Name From Reisende";
            SqlVerbundung ver = new SqlVerbundung();
            ver.VerSQLOpen();
            SqlDataReader reader1 = ver.VerReader(query1);
            while (reader1.Read())
            {
                EinzahlungBeguenstigter3.Items.Add(reader1["Name"]);
                EinzahlungBeguenstigter4.Items.Add(reader1["Name"]);
            }
            ver.VerSQLClose();
            foreach (object item in EinzahlungBeguenstigter3.Items)
            {
                if (item.Equals(EinzahlungSchuldner.SelectedValue))
                {
                    EinzahlungBeguenstigter3.Items.Remove(item);

                    foreach (object item1 in EinzahlungBeguenstigter3.Items)
                    {
                        if (item1.Equals(EinzahlungBeguenstigter2.SelectedValue))
                        {
                            EinzahlungBeguenstigter3.Items.Remove(item1);

                            foreach (object item2 in EinzahlungBeguenstigter3.Items)
                            {
                                if (item2.Equals(EinzahlungBeguenstigter.SelectedValue))
                                {
                                    EinzahlungBeguenstigter3.Items.Remove(item2);
                                    foreach (object item3 in EinzahlungBeguenstigter4.Items)
                                    {
                                        if (item3.Equals(EinzahlungSchuldner.SelectedValue))
                                        {
                                            EinzahlungBeguenstigter4.Items.Remove(item3);

                                            foreach (object item4 in EinzahlungBeguenstigter4.Items)
                                            {
                                                if (item4.Equals(EinzahlungBeguenstigter.SelectedValue))
                                                {
                                                    EinzahlungBeguenstigter4.Items.Remove(item4);

                                                    foreach (object item5 in EinzahlungBeguenstigter4.Items)
                                                    {
                                                        if (item5.Equals(EinzahlungBeguenstigter2.SelectedValue))
                                                        {
                                                            EinzahlungBeguenstigter4.Items.Remove(item5);
                                                            break;
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        // CheckBox auswahl loescht den ausgewaehlte fuer die Nachfolgende Checkbox
        private void EinzahlungBeguenstigter3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            EinzahlungBeguenstigter4.Items.Clear();

            string query1 = "select Name From Reisende";
            SqlVerbundung ver = new SqlVerbundung();
            ver.VerSQLOpen();
            SqlDataReader reader1 = ver.VerReader(query1);
            while (reader1.Read())
            {
                EinzahlungBeguenstigter4.Items.Add(reader1["Name"]);
            }
            ver.VerSQLClose();
            foreach (object item in EinzahlungBeguenstigter4.Items)
            {
                if (item.Equals(EinzahlungSchuldner.SelectedValue))
                {
                    EinzahlungBeguenstigter4.Items.Remove(item);

                    foreach (object item1 in EinzahlungBeguenstigter4.Items)
                    {
                        if (item1.Equals(EinzahlungBeguenstigter.SelectedValue))
                        {
                            EinzahlungBeguenstigter4.Items.Remove(item1);

                            foreach (object item2 in EinzahlungBeguenstigter4.Items)
                            {
                                if (item2.Equals(EinzahlungBeguenstigter2.SelectedValue))
                                {
                                    EinzahlungBeguenstigter4.Items.Remove(item2);

                                    foreach (object item3 in EinzahlungBeguenstigter4.Items)
                                    {
                                        if (item3.Equals(EinzahlungBeguenstigter3.SelectedValue))
                                        {
                                            EinzahlungBeguenstigter4.Items.Remove(item3);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        // CheckBox auswahl loescht den ausgewaehlte fuer die Nachfolgende Checkbox
        private void EinzahlungBeguenstigter4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
