﻿using AbrechungsProgramm.SQlVerbund;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AbrechungsProgramm
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Global Variable
        SqlConnection sqlConnection;
        private string connectionString = ConfigurationManager.ConnectionStrings["AbrechungsProgramm"].ConnectionString;
        public MainWindow()
        {
            //Object erstellung zum verbinden ueber SQLAdapter
            sqlConnection = new SqlConnection(connectionString);
            InitializeComponent();
            SchowReisende();           
        }
        // Anfrage mit Name aus der Combobox und als antwort UserID als int
        private int UserStringInIntConverter(string SelectetValue)
        {
            string ann = SelectetValue;
            string query1 = "exec dbo.ups_Name @Name";
            SqlVerbundung ver = new SqlVerbundung();
            ver.VerSQLOpen();
            SqlDataReader reader1 = ver.VerReaderName(query1, ann);
            int UserID = 0;
            while (reader1.Read())
            {
                UserID = (int)reader1["UserID"];
            }
            ver.VerSQLClose();
            return UserID;
        }
        private void SchowReisende()
        {
            SqlVerbundung ver = new SqlVerbundung();
            string query = "SELECT * FROM Reisende";
            ver.VerSQLOpen();
            SqlDataReader reader = ver.VerReader(query);
            while (reader.Read())
            {
                UserName.Items.Add(reader["Name"]);

            }
            ver.VerSQLClose();
           
        }
        private void SchowGemeinkosten()
        {
            int UserID = UserStringInIntConverter(UserName.SelectedValue.ToString());
            //query Erstellen
            string query = "select  Summe, Notiz From Betrag  WHERE FK_UserID = @ID";
            Komplet.Items.Clear();
            // keine Ahnung
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@ID", UserID);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string wert = $" {reader["Summe"]} € fuer {(string)reader["Notiz"]}";
                        Komplet.Items.Add(wert);
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Info");
                }
            }
        }
        private void SchowEinzelkosten()
        {
            int UserID = UserStringInIntConverter(UserName.SelectedValue.ToString());
            string query = " Select r.Name, s.Betrag, b.Notiz   From Schulden  AS s  Join Betrag AS b on b.BetragID = s.FK_BetragID  Join Reisende AS r on r.UserID  = s.Schuldner Where s.Beguenstigter = @ID";
            Vorderung.Items.Clear();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@ID", UserID);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    int i = 0;
                    while (reader.Read())
                    {
                        string wert = $"{i + 1}.§) {reader["Name"]}  schuldet {reader["Betrag"]} € fuer {reader["Notiz"]} \n";
                        Vorderung.Items.Add(wert);
                        i++;
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            string query1 = " Select res2.Name, ss.Betrag , b.Notiz From Reisende As S join Schulden ss on ss.Schuldner = S.UserID join Reisende AS res2 on  res2.UserID = ss.Beguenstigter join Betrag As b on b.BetragID = ss.FK_BetragID  Where Schuldner = @ID ;";

            Schulden.Items.Clear();
            // keine Ahnung 
            using (SqlConnection connection1 = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query1, connection1);
                command.Parameters.AddWithValue("@ID", UserID);
                try
                {
                    connection1.Open();
                    SqlDataReader reader1 = command.ExecuteReader();
                    int i = 0;
                    while (reader1.Read())
                    {
                        string wert = $" {i + 1}.§ {reader1["Name"]} bekommt {reader1["Betrag"]} €  fuer {reader1["Notiz"]} \n";
                        Schulden.Items.Add(wert);
                        i++;
                    }
                    reader1.Close();
                    connection1.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void UserName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            int UserID = UserStringInIntConverter(UserName.SelectedValue.ToString());

            string query = "select  Summe, Notiz From Betrag  WHERE FK_UserID = @ID";
            Komplet.Items.Clear();

            // keine Ahnung 
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@ID", UserID);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    int i = 0;
                    while (reader.Read())
                    {
                        string wert = $"{i + 1}) {reader["Summe"]} € fuer {reader["Notiz"]}\n";
                        Komplet.Items.Add(wert);
                        i++;

                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    //connection.Close();
                    SchowEinzelkosten();
                }
            }

            Auszuganzeige.Items.Clear();
            string query3 = "exec ups_DatenAusAuszug @ID1";
            SqlVerbundung ver3 = new SqlVerbundung();
            ver3.VerSQLOpen();
            SqlDataReader reader3 = ver3.VerReaderMultiID(query3, UserID);

            while (reader3.Read())
            {
                string sanzeigen = $"{reader3[0]} hat {reader3[1]} am {reader3[2]} an {reader3[3]} beazhlt \n---------------------------------------------------------------------------";
                Auszuganzeige.Items.Add(sanzeigen);
            }
            ver3.VerSQLClose();
        }
        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            Add_User window2 = new Add_User();
            window2.Show();
            this.Close();
        }
        private void AndereBerechnungen_Click(object sender, RoutedEventArgs e)
        {
            Einzahlung blub = new Einzahlung();
            blub.Show();
            this.Close();
        }
        private void AlleBeteiligt(object sender, RoutedEventArgs e)
        {
            AlleBeteiligt alleBeteiligt = new AlleBeteiligt();
            alleBeteiligt.Show();
            this.Close();

        }
        private void ZeigeDieSchuldnerDieserSumme(object sender, SelectionChangedEventArgs e)
        {
            if(Komplet.SelectedValue != null)
            {
                string[] wert = Komplet.SelectedValue.ToString().Split(' ');                    
                decimal  uebergabeZahl = decimal.Parse(wert[1]);
                int id = UserStringInIntConverter(UserName.SelectedValue.ToString());
                Vorderung.Items.Clear();
                SqlVerbundung ver = new SqlVerbundung();
                string query = " usr_ZeigeDieSchuldnerDieAnDiesenBetragGebundenSind @ID, @Betrag";
                ver.VerSQLOpen();
                SqlDataReader reader = ver.ReaderIDUndBetrag(query, id, uebergabeZahl);
            
                while (reader.Read())
                {
                    Vorderung.Items.Add($"{reader[0]} schuldet {reader[1]} fuer {reader[2]}");
                }
                ver.VerSQLClose();
            }
        }
    }
}
