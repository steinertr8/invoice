﻿using AbrechungsProgramm.SQlVerbund;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AbrechungsProgramm
{
    /// <summary>
    /// Interaktionslogik für AlleBeteiligt.xaml
    /// </summary>
   
    public partial class AlleBeteiligt : Window
    {
        public AlleBeteiligt()
        {
            InitializeComponent();
            AkualisiereComboBox();
        }

        private void AkualisiereComboBox()
        {
            AlleUser.Items.Clear();
            string query1 = "select Name From Reisende";
            SqlVerbundung ver = new SqlVerbundung();
            ver.VerSQLOpen();
            SqlDataReader reader1 = ver.VerReader(query1);
            while (reader1.Read())
            {
                AlleUser.Items.Add(reader1["Name"]);
            }
            ver.VerSQLClose();
        }
        private void BackHome(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();
        }
        
        private int UserStringInIntConverter(string SelectetValue)
        {
            string ann = SelectetValue;
            string query1 = "exec dbo.ups_Name @Name";
            SqlVerbundung ver = new SqlVerbundung();
            ver.VerSQLOpen();
            SqlDataReader reader1 = ver.VerReaderName(query1, ann);
            int UserID = 0;
            while (reader1.Read())
            {
                UserID = (int)reader1["UserID"];
            }
            ver.VerSQLClose();
            return UserID;
        }

        private void BuchenUpdate(object sender, RoutedEventArgs e)
        {
            int UserID = UserStringInIntConverter(AlleUser.SelectedValue.ToString());
                   
            if (Betrag.Text != "" && (string) Notize.Text != "" && UserID != 0)
            { 
                try
                {
                    string query = "Insert into Betrag(Summe,Notiz,FK_UserID) Values(@Betrag,  @Notiz, @ID1);";
                    double betrag = double.Parse(Betrag.Text);                    
                    string notize = (string) Notize.Text.ToString();                   
                    SqlVerbundung ver = new SqlVerbundung();
                    ver.VerSQLOpen();
                    ver.VerexecInsertBetrag(query, betrag,  notize, UserID );
                    ver.VerSQLClose(); 
                
                    if (Betrag.Text != null)
                    {
                        double wert = double.Parse(Betrag.Text);
                        Berechnung(wert);
                        Betrag.Text = "";
                        Notize.Text = "";
                    }
                }
                catch (Exception exeptoion)
                {
                    MessageBox.Show(exeptoion.Message,"BuchungUpdate");
                }
            }
            else
            {
                MessageBox.Show("Ups da hat etwas nicht geklapt", "info");
            }
        }
        private double Berechnung(double ergebnis)
        {
            int b = 0;
            string schulnderID = "";
            foreach (object item in AlleUser.Items)
            {
                if (!item.Equals(AlleUser.SelectedItem))
                {
                    schulnderID += item;
                }
                b++;
            }
            ergebnis = ergebnis / b;
            SchuldenEinsetzen(ergebnis);
            return ergebnis;
        }
        private void SchuldenEinsetzen(double ergebnis)
        {
            try 
            { 
                int UserID = UserStringInIntConverter(AlleUser.SelectedValue.ToString());
                foreach (object item in AlleUser.Items)
                {
                    if (!item.Equals(AlleUser.SelectedItem))
                    {
                        string ann = item.ToString();
                        string query1 = "exec dbo.ups_Name @Name";
                        SqlVerbundung ver = new SqlVerbundung();

                        ver.VerSQLOpen();
                        SqlDataReader reader1 = ver.VerReaderName(query1, ann);
                        int schuldID = 0;
                        while (reader1.Read())
                        {
                            schuldID = (int)reader1["UserID"];
                        }
                        ver.VerSQLClose();

                        string query = "Insert into Schulden( Schuldner, Beguenstigter, Betrag, FK_BetragID) Values(@ID1,@ID2,@Betrag,IDENT_CURRENT('Betrag')); ";
                        SqlVerbundung verbindung = new SqlVerbundung();
                        verbindung.VerSQLOpen();
                        verbindung.VerexecMulti(query,schuldID, UserID, ergebnis);
                        verbindung.VerSQLClose();
                    }
                }
            }
            catch (Exception exeptoion)
            {
                MessageBox.Show(exeptoion.Message, "ja2");
            }            
        }
    }
}
