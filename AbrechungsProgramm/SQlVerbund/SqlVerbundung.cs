﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace AbrechungsProgramm.SQlVerbund
{
    internal class SqlVerbundung
    {

        //private SqlConnection sqlConnection;
        //private string connectionString = ConfigurationManager.ConnectionStrings["AbrechungsProgramm"].ConnectionString;

        // string connectionString = ConfigurationManager.ConnectionStrings["AbrechungsProgramm"].ConnectionString;
        //sqlConnection = new SqlConnection(connectionString);


        //Erstellen Sie eine variable Zeichenfolge zum Speichern der Verbindungszeichenfolge und ein SqlConnection-Objekt: 
        string ConnectionString = ConfigurationManager.ConnectionStrings["AbrechungsProgramm"].ConnectionString;
        SqlConnection con;

        /// <summary>
        ///Erstellen Sie eine Methode zum Öffnen der Verbindung.Zuerst mache ich eine Methode OpenConnection()
        /// für den Zugriff auf den Datenbankaufruf ganz am Anfang, um die Datenbankverbindung zu öffnen.
        /// </summary>
        public void VerSQLOpen()
        {
            con = new SqlConnection(ConnectionString);
            con.Open();
        }
        // diese schließt die Verbindung
        public void VerSQLClose()
        {
            con.Close();
        }
        // Löschen Abfragen aktualliesieren Einfügen
        public void Verexec(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.ExecuteNonQuery();
        }
        public void VerexecSingel(string Query_,string name)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@Name", name);
            cmd.ExecuteNonQuery();
        }

        public void VerexecMulti(string Query_, int? SchuldnerID1 = null, int? BegunstrigterID2 = null, double betrag = 0)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID1", SchuldnerID1);
            cmd.Parameters.AddWithValue("@ID2", BegunstrigterID2);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.ExecuteScalar();
        }
        public void VerexecOhneNotizID(string Query_, int SchuldnerID1, int BegunstrigterID2, double betrag, string notiz)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID1", SchuldnerID1);
            cmd.Parameters.AddWithValue("@ID2", BegunstrigterID2);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.Parameters.AddWithValue("@Notiz", notiz);
            cmd.ExecuteScalar();
        }
        public void VerexecOhneNotizIDEiner(string Query_, int SchuldnerID1, int BegunstrigterID2, double betrag, double betrag1, string notiz)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID", SchuldnerID1);
            cmd.Parameters.AddWithValue("@ID1", BegunstrigterID2);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.Parameters.AddWithValue("@Betrag1", betrag1);
            cmd.Parameters.AddWithValue("@Notiz", notiz);
            cmd.ExecuteScalar();
        }


        public void VereinzelteVeerteilungZwei(string Query_, int SchuldnerID, int SchuldnerID1, int BegunstrigterID2, double betrag, double betrag1, double betrag2, string notiz)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID", SchuldnerID);
            cmd.Parameters.AddWithValue("@ID1", SchuldnerID1);
            cmd.Parameters.AddWithValue("@ID2", BegunstrigterID2);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.Parameters.AddWithValue("@Betrag1", betrag1);
            cmd.Parameters.AddWithValue("@Betrag2", betrag2);
            cmd.Parameters.AddWithValue("@Notiz", notiz);
            cmd.ExecuteScalar();
        }
        public void VereinzelteVeerteilungDrei(string Query_, int SchuldnerID, int SchuldnerID1, int BegunstrigterID2, int BegunstrigterID3, double betrag, double betrag1, double betrag2, double betrag3, string notiz)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID", SchuldnerID);
            cmd.Parameters.AddWithValue("@ID1", SchuldnerID1);
            cmd.Parameters.AddWithValue("@ID2", BegunstrigterID2);
            cmd.Parameters.AddWithValue("@ID3", BegunstrigterID3);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.Parameters.AddWithValue("@Betrag1", betrag1);
            cmd.Parameters.AddWithValue("@Betrag2", betrag2);
            cmd.Parameters.AddWithValue("@Betrag3", betrag3);
            cmd.Parameters.AddWithValue("@Notiz", notiz);
            cmd.ExecuteScalar();
        }
        public void VereinzelteVeerteilungVier(string Query_, int SchuldnerID, int BegunstrigterID1, int BegunstrigterID2, int BegunstrigterID3, int BegunstrigterID4, double betrag, double betrag1, double betrag2, double betrag3, double betrag4, string notiz)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID", SchuldnerID);
            cmd.Parameters.AddWithValue("@ID1", BegunstrigterID1);
            cmd.Parameters.AddWithValue("@ID2", BegunstrigterID2);
            cmd.Parameters.AddWithValue("@ID3", BegunstrigterID3);
            cmd.Parameters.AddWithValue("@ID4", BegunstrigterID4);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.Parameters.AddWithValue("@Betrag1", betrag1);
            cmd.Parameters.AddWithValue("@Betrag2", betrag2);
            cmd.Parameters.AddWithValue("@Betrag3", betrag3);
            cmd.Parameters.AddWithValue("@Betrag4", betrag4);
            cmd.Parameters.AddWithValue("@Notiz", notiz);
            cmd.ExecuteScalar();
        }
        
        public void UpdateMitFK_BetragID(string Query_, int SchuldnerID1, int BegunstrigterID2, double betrag, int FK_BetragID)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID1", SchuldnerID1);
            cmd.Parameters.AddWithValue("@ID2", BegunstrigterID2);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.Parameters.AddWithValue("@FK_BetragID", FK_BetragID);
            cmd.ExecuteScalar();
        }
        public void VerexecInsertBetrag(string Query_, double betrag , string Notiz, int SchuldnerID1 )
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@Betrag", betrag);
            cmd.Parameters.AddWithValue("@Notiz", Notiz);
            cmd.Parameters.AddWithValue("@ID1", SchuldnerID1);
            cmd.ExecuteScalar();
        }
        // Reder bearbeiten
        public SqlDataReader VerReader(string Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
        public SqlDataReader VerReaderName(string Query_, string Name)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@Name", Name);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public SqlDataReader VerReaderNotiz(string Query_, string Notiz)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@Notiz", Notiz);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
        public SqlDataReader VerReaderMultiID(string Query_, int Notiz)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID1", Notiz);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        } 
        public SqlDataReader ReaderIDUndBetrag(string Query_, int id, decimal zahl)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.Parameters.AddWithValue("@ID", id);
            cmd.Parameters.AddWithValue("@Betrag", zahl);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }



        // Object Bearbeiten
        public object ZeigGridView(string Query_)
        {
            SqlDataAdapter dr = new SqlDataAdapter(Query_, ConnectionString);
            DataSet ds = new DataSet();
            dr.Fill(ds);
            object dataum = ds.Tables[0];
            return dataum;
        }
    }
}
