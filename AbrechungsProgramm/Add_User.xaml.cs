﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AbrechungsProgramm.SQlVerbund;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;


namespace AbrechungsProgramm
{
    /// <summary>
    /// Interaktionslogik für Add_User.xaml
    /// </summary>
    public partial class Add_User : Window
    {
        public Add_User()
        {
            InitializeComponent();

        }
        
        private void ReisendeErstellen(object sender, RoutedEventArgs e)
        {
            if (NameVonMitreisenden.Text != "")
            {
                //MainWindow newWindow = new MainWindow();
                string query = "insert into Reisende Values (@Name)";
                SqlVerbundung ver = new SqlVerbundung();
                ver.VerSQLOpen();
                ver.VerexecSingel(query, NameVonMitreisenden.Text);
                ver.VerSQLClose();
                //string query1 = "SELECT * FROM Reisende";
                //ver.VerSQLOpen();
                //SqlDataReader reader = ver.VerReader(query1);
                //while (reader.Read())
                //{
                //    newWindow.UserName.Items.Add(reader["Name"]);
                //}
                ver.VerSQLClose();
                MessageBoxResult resalt = MessageBox.Show("Moechten Sie weiter Reisende einfuegen ??", "info", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (MessageBoxResult.No == resalt)
                {
                    MainWindow newWindow = new MainWindow();
                    //Application.Current.MainWindow = newWindow;
                    newWindow.Show();
                    this.Close();
                } 
            }
            else
            {
                MessageBox.Show("Bitte Name eingeben");
            }
            NameVonMitreisenden.Text = "";

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWindow = new MainWindow();
            newWindow.Show();
            this.Close();

        }
    }
}
