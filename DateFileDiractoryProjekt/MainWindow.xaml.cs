﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows;
using System.Runtime.InteropServices;
using Path = System.IO.Path;
using Aspose.Cells.Drawing;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.FileIO;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection;
using TextBox = System.Windows.Forms.TextBox;
using System.Diagnostics;
using static System.Windows.Forms.LinkLabel;
using MessageBox = System.Windows.Forms.MessageBox;
using System.Globalization;
using System.Threading;

namespace DateFileDiractoryProjekt
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            z();
        }
        private void z()
        {
            DriveInfo[] arrDrives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in arrDrives.OrderBy(f => f.Name))
            {
                if (drive.IsReady)
                {
                    string sDrive = drive.Name;
                    Laufwerke.Items.Add(sDrive);
                }
            }
        }
        private void Load_Drivers(object sender, SelectionChangedEventArgs e)
        {
            SubFolder.Text = "";
            CreateFolderInVerzeichnisDirectory.Text = "";
            FolderSearch.Items.Clear();
            FolderSearchNext.Items.Clear();
            if (Laufwerke.SelectedValue != null)
            {
                try
                {
                    driversInformation.Text = "";
                    FolderSearch.Items.Clear();
                    FolderSearchNext.Items.Clear();
                    DirectoryInfo d = new DirectoryInfo(Laufwerke.SelectedValue.ToString());
                    foreach (object item in d.GetDirectories())
                    {
                        FolderSearch.Items.Add(item);
                    }
                    string blubInfo = "";
                    DriveInfo[] allDrives = DriveInfo.GetDrives();
                    foreach (DriveInfo drive in allDrives)
                    {
                        if (drive.IsReady == true)
                        {
                            if (drive.Name.Equals(Laufwerke.SelectedValue.ToString()))
                            {
                                blubInfo += $"{drive.VolumeLabel}\n {drive.Name}\t{String.Format("{0:#.## Gb}", (Math.Round((decimal)drive.TotalSize / 1024 / 1024 / 1024)))} \t Driver Total \n " +
                                    $" Freierspeicherplatz\t  {String.Format("{0:#.## Gb}", (Math.Round((decimal)drive.TotalFreeSpace / 1024 / 1024 / 1024)))}  \n Format  {drive.DriveFormat} \n Typ {drive.DriveType}";
                            }
                        }
                    }
                    string[] groessen = blubInfo.Split(' ');
                    driversInformation.Text += blubInfo;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }
        private void Load_Folder(object sender, SelectionChangedEventArgs e)
        {
            if (FolderSearch.SelectedValue != null)
            {
                try
                {
                    FolderSearchNext.Items.Clear();
                    string Pfad = Laufwerke.SelectedValue.ToString();
                    Pfad += FolderSearch.SelectedValue.ToString();
                    string Pfad1 = FolderSearch.SelectedValue.ToString();

                    DirectoryInfo d = new DirectoryInfo(Pfad);
                    foreach (object item in d.GetDirectories())
                    {
                        FolderSearchNext.Items.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }
        private void Load_FolderNext(object sender, SelectionChangedEventArgs e)
        {
            if (FolderSearchNext.SelectedValue != null)
            {
                SubFolder.Text = "";
                CreateFolderInVerzeichnisDirectory.Text = "";
                string Pfad = Laufwerke.SelectedValue.ToString();
                Pfad += FolderSearch.SelectedValue.ToString() + "\\";
                Pfad += FolderSearchNext.SelectedValue.ToString() + "\\NewFolder";
                CreateFolderInVerzeichnisDirectory.Text += Pfad;
                SubFolder.Text += Pfad + "\\SubFolder";
            }


        }
        private void Button_ClickCreateDiractory(object sender, RoutedEventArgs e)
        {
            if (CreateFolderInVerzeichnisDirectory.Text != "")
            {
                try
                {
                    if (!Directory.Exists(CreateFolderInVerzeichnisDirectory.Text.ToString()))
                    {
                        Directory.CreateDirectory(CreateFolderInVerzeichnisDirectory.Text.ToString());
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
                CreateFolderInVerzeichnisDirectory.Text = "";
            }
            else if (CreateFolderInVerzeichnisDirectory.Text == "")
            {
                try
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.Description = "Directory Source";
                    fbd.ShowDialog();
                    string pfadDiroctory = fbd.SelectedPath;
                    string temp = Interaction.InputBox("Create Name for Directory...", "Directory Name", "");
                    pfadDiroctory += "\\" + temp;
                    CreateFolderInVerzeichnisDirectory.Text = pfadDiroctory;
                    if (!Directory.Exists(CreateFolderInVerzeichnisDirectory.Text.ToString()))
                    {
                        Directory.CreateDirectory(CreateFolderInVerzeichnisDirectory.Text.ToString());
                    }

                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }

                MessageBoxResult result = System.Windows.MessageBox.Show("Directory Create again ??", "Info", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (MessageBoxResult.Yes == result)
                {
                    Button_ClickCreateDiractory(sender, e);
                }
                MessageBoxResult result1 = System.Windows.MessageBox.Show("Let them create subdirectory ??", "Info", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (MessageBoxResult.Yes == result1)
                {
                    Button_CreateSubFolder(sender, e);
                }
                SubFolder.Text = "";
                CreateFolderInVerzeichnisDirectory.Text = "";
            }
        }
        private void Button_CreateSubFolder(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Directory.Exists(SubFolder.Text.ToString()))
                {
                    Directory.CreateDirectory(SubFolder.Text.ToString());
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
            SubFolder.Text = "";
        }
        private void ShowDirectoryFile(object sender, RoutedEventArgs e)
        {
            FileSammler.Items.Clear();
            try
            {
                if (CreateFolderInVerzeichnisDirectory.Text.ToString() == "")
                {

                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.ShowDialog();
                    string path = fbd.SelectedPath;
                    string[] files = Directory.GetFiles(path);
                    foreach (string file in files)
                    {
                        string[] textdateiName = file.Split('\\');
                        FileSammler.Items.Add(textdateiName.Last());
                    }
                }
                else
                {
                    string path1 = CreateFolderInVerzeichnisDirectory.Text.ToString();
                    string[] files1 = Directory.GetFiles(path1);
                    foreach (string file1 in files1)
                    {
                        string[] textdateiName = file1.Split('\\');
                        FileSammler.Items.Add(textdateiName.Last());
                    }
                }

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
        private void CreateFile_button(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CreateFileTextBox.Text != "")
                {
                    string filePfad = CreateFileTextBox.Text.ToString();
                    FileInfo file1 = new FileInfo(filePfad);
                    if (!file1.Exists)
                    {
                        file1.Create();
                        CreateFileTextBox.Text = "";
                    }
                }
                else
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.ShowDialog();
                    CreateFileTextBox.Text = fbd.SelectedPath + "\\filename";

                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
        private void Button_CopyDirictory(object sender, RoutedEventArgs e)
        {
            try
            {
                if (VorlageFuerCopy.Text != null)
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.Description = "Directory Source";
                    fbd.ShowDialog();
                    VorlageFuerCopy.Text = fbd.SelectedPath;
                }
                if (CopyVonVorlage.Text != null)
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.Description = "Destribution of Directory Copy";
                    fbd.ShowDialog();
                    CopyVonVorlage.Text = fbd.SelectedPath;
                    string sourceDir = VorlageFuerCopy.Text;
                    string PfadestinationDird = CopyVonVorlage.Text;
                    CopyDirectory(sourceDir, PfadestinationDird);
                    CopyVonVorlage.Text = "";
                    VorlageFuerCopy.Text = "";
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

        }
        static void CopyDirectory(string sourceDir, string destinationDir)
        {
            var dir = new DirectoryInfo(sourceDir);

            if (!dir.Exists) throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");
            string temp = Interaction.InputBox("Copy in ...", "Name for Directory", "");
            string wertre = "\\" + temp;
            destinationDir += wertre;
            Directory.CreateDirectory(destinationDir);

            foreach (FileInfo file in dir.GetFiles())
            {
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
            }
        }
        private void RenamediesenFille(object sender, RoutedEventArgs e)
        {
            try
            {
                // Von 
                string oldPath = OpenfileTextBox.Text.ToString();
                string newName = OpenFileTextBoxRename.Text.ToString();

                if (String.IsNullOrEmpty(oldPath))
                    throw new ArgumentNullException("oldPath");
                if (String.IsNullOrEmpty(newName))
                    throw new ArgumentNullException("newName");

                string oldName = Path.GetFileName(oldPath);

                // if the file name is changed 
                if (!String.Equals(oldName, newName, StringComparison.CurrentCulture))
                {
                    string folder = Path.GetDirectoryName(oldPath);
                    string newPath = Path.Combine(folder, newName);
                    bool changeCase = String.Equals(oldName, newName, StringComparison.CurrentCultureIgnoreCase);

                    // if renamed file already exists and not just changing case 
                    if (File.Exists(newPath) && !changeCase)
                    {
                        throw new IOException(String.Format("File already exists:n{0}", newPath));
                    }
                    else if (changeCase)
                    {
                        // Move fails when changing case, so need to perform two moves
                        string tempPath = Path.Combine(folder, Guid.NewGuid().ToString());
                        Directory.Move(oldPath, tempPath);
                        Directory.Move(tempPath, newPath);
                    }
                    else
                    {
                        Directory.Move(oldPath, newPath);
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("The name exists ");
                }
                OpenfileTextBox.Text = "";
                OpenFileTextBoxRename.Text = "";
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }
        private void Openfelibox(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fbd = new OpenFileDialog();
            fbd.ShowDialog();
            OpenfileTextBox.Text = fbd.FileName.ToString();
            OpenFileTextBoxRename.Text = fbd.FileName.ToString();
        }
        private void Write_Button(object sender, RoutedEventArgs e)
        {
            if (OpenfileTextBox.Text != null)
            {
                // Frage Ueberschreiben
                MessageBoxResult result = System.Windows.MessageBox.Show("File with your text overwrite ??", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (MessageBoxResult.Yes == result)
                {
                    string pfad = OpenfileTextBox.Text;
                    using (StreamWriter writetext = new StreamWriter(pfad))
                    {
                        writetext.WriteLine(WriteBox.Text);
                    }
                }
                if (MessageBoxResult.No == result)
                {
                        //Frage anknuepfen??
                        MessageBoxResult result1 = System.Windows.MessageBox.Show("Add text to the file ??", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (MessageBoxResult.Yes == result1)
                    {
                        string pfad1 = OpenfileTextBox.Text;
                        using (StreamWriter writetext = File.AppendText(pfad1))
                        {
                            writetext.WriteLine(WriteBox.Text);
                        }
                    }
                }
            }
        }
        private void Read_Button(object sender, RoutedEventArgs e)
        {
            ReadBox.Text = "";
            UpdateBox.Text = "";
            WriteBox.Text = "";
            if (OpenfileTextBox.Text != null)
            {
                try
                {
                    string pfadFile = OpenfileTextBox.Text;
                    FileInfo file1 = new FileInfo(pfadFile);
                    if (file1.Exists)
                    {
                        string pfad = OpenfileTextBox.Text;
                        string[] lines = System.IO.File.ReadAllLines(pfad);
                        foreach (string line in lines)
                        {
                            ReadBox.Text += line;
                            UpdateBox.Text += line;
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }
        private void Find_button(object sender, RoutedEventArgs e)
        {
            string word = FindBox.Text;

            //System.Windows.MessageBox.Show(inde.ToString());

            ReadBox.Focus();
            ReadBox.Select(ReadBox.Text.IndexOf(word), word.Length);
            
            
            
            if (ReadBox.Text.Contains(FindBox.Text))
            {                
                int i = 0;
                foreach (string item in ReadBox.Text.Split(' '))
                {
                    if (item.Equals(FindBox.Text)) i++;
                }
                word += $"  found {i} ,  rename  ?";
                string temp = Interaction.InputBox(word, "Rename ..");
                if (temp != "") ReadBox.Text = ReadBox.Text.Replace(FindBox.Text, temp);
            }
        }
        private void Update_Button(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = System.Windows.MessageBox.Show("File with your update overwrite ??", "Info", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (MessageBoxResult.Yes == result)
            {
                string pfad = OpenfileTextBox.Text;
                using (StreamWriter writetext = new StreamWriter(pfad))
                {
                    writetext.WriteLine(WriteBox.Text);

                }
            }
        }
        private void Update(object sender, RoutedEventArgs e)
        {
            UpdateBox.TextChanged += this.UpdateBox_TextChanged;
            
        }
        private void UpdateBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ReadBox.Text = String.Format(UpdateBox.Text);
            WriteBox.Text = String.Format(UpdateBox.Text);
        }       
    }
}

































//// Öfenen und lesen
//string adres = @"C:\Users\R_Steinert\Desktop\TestfuerC#.txt";
//StreamReader lese = new StreamReader(adres);           
//string text = lese.ReadToEnd();
//lese.Close();


//// Öfnen und schreiben datei Name wird erstellt
//string adres1 = @"C:\Users\R_Steinert\Desktop\TestCopyfuerC#.txt";
//StreamWriter writer = new StreamWriter(adres1,true); 

//writer.WriteLine($"Hallo wir beschreiben diese Datei ---->>{text} <<----Diese datei ");
//writer.Close();
////Console.WriteLine(text);
//string adres2 = @"C:\Users\R_Steinert\Desktop\TestCopy1fuerC#.txt";
//FileInfo file = new FileInfo(adres2);

//file.Create();
//if (file.Exists)
//{
//    string fullPfad = file.FullName;
//}
/////
////-----------Flievernichtung            
/////

////file.Delete();
////File.Delete(file.Name);

//DriveInfo disc1 = new DriveInfo(@"Y:\");
//disc1.RootDirectory.Delete();
//Console.WriteLine(disc1);


//DirectoryInfo directory = new DirectoryInfo(@"U:\Lisa_Kurz");
//directory.Delete();

//List<Json> list = new List<Json>();
//list.Add();

//List<JsonBig> list1 = new List<JsonBig>();
//list.Add();


//string json = JsonSerializer.Serialize(list);
//File.WriteAllText(@"C:\Users\R_Steinert\Desktop\jsonTest.json", json);

//string json2 = File.ReadAllText(@"C:\Users\R_Steinert\Desktop\jsonTest.json");
//var outpu = JsonSerializer.Deserialize<List<Json>>(json2);

