﻿namespace Hangman
{
    internal class HangmanSpielplatz
    {
        static void Main(string[] args)
        {
            MainMenu();
        }
        static void MainMenu()
        {


            Console.WriteLine("Hello, World!\n### HANGMAN ###\n###############");
            Console.WriteLine("Wähle eine Aktion aus \n");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("[1] Spielen\n[2] Exit");
            Console.ResetColor();
            Console.WriteLine("Aktion");
            StartGame();
        }
        static void StartGame()
        {
            char a = Console.ReadKey().KeyChar;
            if (a == '1')
            {
                string[] words = new string[]
                {
                    "Widerholung",
                    "Fohrtfahren",
                    "Inkrement",
                    "Planung",
                    "Implemantation",
                    "Projektmanagement",
                    "Fussballspieler",
                    "Vererbungsplanungsverfahren",
                    "Schleifenspetzi"
                };
                Random random = new Random();
                string b = words[random.Next(words.Length)];
                string v = "";

                for (int i = 0; i < b.Length; i++)
                {
                    v += b[i];
                }
                string großBuchstaben = v.ToUpper();
                GameLoop(großBuchstaben);
            }
            else if (a == '2')
            {
                Environment.Exit(-1);
            }
            else
            {
                Console.WriteLine("Hörauf gib die richtige Zahl ein");
                MainMenu();
            }
        }
        static void GameLoop(string word)
        {
            Console.Clear();
            string ver = "";
            for (int i = 0; i < word.Length; i++)
            {
                ver += "_";
            }
            string gehabt = "";
            int versuchen = 0;



            while (versuchen < 10 && ver != word)
            {

                Console.WriteLine("\n");
                Console.WriteLine("Gesuchtes Word:  " + ver);
                Console.WriteLine("Anzahlder Buchsatben:  " + ver.Length);
                Console.WriteLine($"Du hast 10 versuche  {versuchen} \n");
                Console.WriteLine("Buchstaben wählen: \n");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"die hast schon {gehabt} versuche");
                Console.ResetColor();
                bool treffer = false;
                char a = Console.ReadKey().KeyChar;
                char b = char.ToUpper(a);
                Console.WriteLine("\n");
                string kopie = "";

                for (int i = 0; i < word.Length; i++)
                {
                    if (b == word[i])
                    {
                        treffer = true;
                        kopie += b;
                    }
                    else
                    {
                        kopie += ver[i];
                    }
                }
                if (treffer == false)
                {

                    versuchen++;
                    gehabt += b + ",";
                    Console.WriteLine($"du hast noch ca. {versuchen} versuche");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"die hast schon {gehabt} versuche");
                    Console.ResetColor();

                }

                ver = kopie;
                Console.WriteLine("Gesuchtes Word:  " + ver);
                Console.WriteLine("Anzahlder Buchsatben:  " + ver.Length);
                Console.WriteLine("Noch übrige Versuche : \n");
                Console.WriteLine(versuchen + "\n");
                Console.WriteLine("Buchstaben wählen: \n");
                Console.Clear();
            }
            if (versuchen >= 10)
            {
                Console.WriteLine("Verloren");
                Console.WriteLine("Spielnochmal spielen ??? >> Y << fuer ja ... alles andere zum abruch ");
                if (Console.ReadKey().KeyChar == 'y' || Console.ReadKey().KeyChar == 'Y')
                {
                    Console.WriteLine("\n");
                    MainMenu();
                }
                else
                {
                    Environment.Exit(-1);
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Gewonnen");
                Console.WriteLine("Spielnochmal spielen ??? >> Y << fuer ja ... alles andere zum abruch ");
                if (Console.ReadKey().KeyChar == 'y' || Console.ReadKey().KeyChar == 'Y')
                {
                    Console.WriteLine("\n");
                    MainMenu();
                }
                else
                {
                    Environment.Exit(-1);
                }

            }
        }
    }
}