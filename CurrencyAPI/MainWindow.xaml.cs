﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Reflection.Emit;

namespace CurrencyAPI
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }       
        private async void Grid_LoadedAsync(object sender, RoutedEventArgs e)
        {            
            DispatcherTimer timer = new DispatcherTimer(TimeSpan.FromSeconds(1), DispatcherPriority.Normal, (object s, EventArgs ev) =>
            {
                this.BoxDatumZeit.Text = DateTime.Now.ToString("hh:mm:ss");
            }, 
            this.Dispatcher);
            timer.Start();

            string dateTime = DateTime.Now.ToString("dd. MMMM. yyyy");  
            
            HttpClient httpClient1 = new HttpClient();
            var result1 = await httpClient1.GetAsync($"und hier steht url adresse");
                result1.EnsureSuccessStatusCode();
            if (result1 != null && (int)result1.StatusCode == 200 && result1.ReasonPhrase == "OK")
            {
                if (BoxDatum != null )
                {
                    BoxDatum.Text += dateTime;                   
                }
                string er = await result1.Content.ReadAsStringAsync();
                string pattern = @"\b[A-Z][A-Z][A-Z]\b";                
                foreach(Match match in Regex.Matches(er, pattern))
                {
                    if (match.Success)
                    {
                        combo1.Items.Add( match.ToString());
                        combo2.Items.Add( match.ToString());
                    }
                }
            }
        }   
        private async void Button_Click(object sender, RoutedEventArgs e)
        {   
                //auslesen von den vorgaben
                string value1 = (string)combo1.SelectionBoxItem;           
                string value2 = (string)combo2.SelectionBoxItem;
                string amount = (string)Box2.Text;
            
            if (combo1.SelectionBoxItem != null && combo2.SelectionBoxItem != null && Box2.Text != null && double.TryParse(amount, out _))
            {
                    //date
                    string dateTime = DateTime.Now.ToString("yyyy-MM-dd");

                    // Object fuer request an die Api die bezahlt ist fuer 100 request
                    HttpClient httpClient = new HttpClient();
                
                    // Umsetzung des Request            
                    var result2 = await httpClient.GetAsync(
                        $"und hier steht die zweite adresse oder so");       
                    result2.EnsureSuccessStatusCode();
                    if(result2 != null && (int)result2.StatusCode == 200 && result2.ReasonPhrase == "OK")
                    {
                        string er = await result2.Content.ReadAsStringAsync();
                        string[] subs = er.Split('"');               
                        string b = "";
                        string bq = "";
                        for(int i =0; i<subs.Length; i++)
                        {
                            if (subs[i].Equals("rate_for_amount"))
                            {
                                b += subs[i+2];
                            }
                            if (subs[i].Equals("rate"))
                            {
                            bq = $" 1 {value1}  entspricht  {value2}  {subs[i + 2]} "; 
                      
                            }
                        }
                            BoxKurse.Text += bq; 
                            Box1.Text += b;            
                    }                
            }            
        }
        private void FelderLeeren(object sender, SelectionChangedEventArgs e)
        {
            BoxKurse.Clear();
            Box1.Clear();
            Box2.Clear();
        }        
    }
}
