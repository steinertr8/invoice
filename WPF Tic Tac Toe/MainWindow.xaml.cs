﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Tic_Tac_Toe
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           
        }
        private bool isCircel = false;
        private void buttons_clik(object sender, RoutedEventArgs e)
        {
          // (Button)sender = Brushes.Violet;
            SetTetx((Button)sender);
            CheckResult((string)((Button)sender).Content);
        }

        private void ResetAll()
        {
            List<Button> list = new List<Button>();

            list.Add(btn1); list.Add(btn2); list.Add(btn3); list.Add(btn4);
            list.Add(btn5); list.Add(btn6); list.Add(btn7); list.Add(btn8);
            list.Add(btn9);
            foreach (Button button in list)
            {
                button.IsEnabled = true;
                button.Content = "";
               
            }
        }        

        private void SetTetx(Button b)
        {
            if (isCircel)
            {
                b.Content = "O";
                b.IsEnabled = false;              
                b.FontSize = 120;
                isCircel = false;
            }
            else
            {
                b.Content = "X";
                b.IsEnabled = false;
                b.FontSize = 120;
                isCircel = true;
            }
        }
        private void CheckResult(string x)
        {
            bool r = false;
            if (btn1.Content == btn2.Content && btn1.Content == btn3.Content && (string)btn3.Content != "")
            {
                r = true;
            }
            if (btn4.Content == btn5.Content && btn4.Content == btn6.Content && (string)btn6.Content != "")
            {
                r = true;
            }
            if (btn7.Content == btn8.Content && btn7.Content == btn9.Content && (string)btn9.Content != "")
            {
                r = true;
            }
            if (btn1.Content == btn4.Content && btn1.Content == btn7.Content && (string)btn4.Content != "")
            {
                r = true;
            }
            if (btn2.Content == btn5.Content && btn2.Content == btn8.Content && (string)btn8.Content != "")
            {
                r = true;
            }
            if (btn3.Content == btn6.Content && btn3.Content == btn9.Content && (string)btn3.Content != "")
            {
                r = true;
            }
            if (btn1.Content == btn5.Content && btn1.Content == btn9.Content && (string)btn5.Content != "")
            {
                r = true;
            }
            if (btn3.Content == btn5.Content && btn3.Content == btn7.Content && (string)btn7.Content != "")
            {
                r = true;
            }
            if (r)
            {
                SetWinner(x);
            }
            else if ((string)btn4.Content != "" && (string)btn5.Content != "" && (string)btn6.Content != "" && (string)btn7.Content != "" && (string)btn8.Content != ""
                && (string)btn9.Content != "" && (string)btn1.Content != "" && (string)btn2.Content != "" && (string)btn3.Content != "")
            {
                MessageBox.Show("Drow : Drow");
                MessageBoxResult result = MessageBox.Show("Nochmal", "Spiel", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    //MainWindow spiel = new MainWindow();
                    //spiel.Show();
                    //this.Close();
                    ResetAll();
                }
            }
        }
        private void SetWinner(string x)
        {
            string winner = x;
            MessageBox.Show("Winner : " + winner);
            MessageBoxResult result = MessageBox.Show("Nochmal","Spiel",MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                //MainWindow spiel = new MainWindow();
               // spiel.Show();
               // this.Close();
                ResetAll();
            }
        }
    }
}
